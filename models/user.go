package models

type User struct {
	Id        uint   `gorm:"primaryKey" gorm:"autoIncrement"`
	FirstName string `gorm:"not null"`
	LastName  string `gorm:"not null"`
	Email     string `gorm:"not null" gorm:"unique"`
	Password  string `gorm:"not null"`
	No_hp     string `gorm:"not null"`
	UpdatedAt int64  `gorm:"autoUpdateTime:milli"` // Use unix milli seconds as updating time
	CreatedAt int64  `gorm:"autoCreateTime"`
}
