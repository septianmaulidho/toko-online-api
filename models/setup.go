package models

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func SetupCon() *gorm.DB {
	// refer https://github.com/go-sql-driver/mysql#dsn-data-source-name for details
	dsn := "root:123123@tcp(127.0.0.1:3306)/toko_online?charset=utf8&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("gagal koneksi database")
	}

	db.AutoMigrate(&User{})

	return db
}
