package controllers

import (
	"net/http"
	"time"
	"tokoOnlineAPI/models"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type RegisterBody struct {
	FirstName string
	LastName  string
	Email     string
	Password  string
	No_hp     string
}

type AuthResponse struct {
	Id        uint
	FirstName string
	LastName  string
	Email     string
	No_hp     string
	Token     string
}

type LoginForm struct {
	Email    string
	Password string
}

var signingKey = []byte("mysecretkey743748")

func Register(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var dataInput RegisterBody

	if err := c.ShouldBindJSON(&dataInput); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	hashsedPassword, _ := bcrypt.GenerateFromPassword([]byte(dataInput.Password), 10)
	user := models.User{
		FirstName: dataInput.FirstName,
		LastName:  dataInput.LastName,
		Email:     dataInput.Email,
		Password:  string(hashsedPassword),
		No_hp:     dataInput.No_hp,
	}

	db.Create(&user)
	token, err := GenerateToken(user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Error while generate token!"})
		return
	}
	response := AuthResponse{
		Id:        user.Id,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		No_hp:     user.No_hp,
		Token:     token,
	}

	c.JSON(http.StatusOK, gin.H{"data": response})
}

func Login(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var dataInput LoginForm

	if err := c.ShouldBindJSON(&dataInput); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var user models.User

	if err := db.Where("email = ?", dataInput.Email).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Data user tidak ditemukan"})
		return
	}

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(dataInput.Password))

	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{
			"code":   401,
			"errors": "Wrong Password!"})
		return
	}

	token, err := GenerateToken(user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code":    500,
			"message": "Error while generate token!"})
		return
	}
	response := AuthResponse{
		Id:        user.Id,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		No_hp:     user.No_hp,
		Token:     token,
	}

	c.JSON(http.StatusOK, gin.H{"data": response})

}

func GenerateToken(user models.User) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["Id"] = user.Id
	claims["FirstName"] = user.FirstName
	claims["LastName"] = user.LastName
	claims["Email"] = user.Email
	claims["No_hp"] = user.No_hp
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	tokenString, err := token.SignedString(signingKey)

	if err != nil {
		// fmt.Errorf("Something went wrong: %s", err.Error())
		return "", err
	}

	return tokenString, nil

}
