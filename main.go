package main

import (
	"net/http"
	"tokoOnlineAPI/controllers"
	"tokoOnlineAPI/models"

	"github.com/gin-gonic/gin"
)

func main() {

	r := gin.Default()

	// MODEL
	db := models.SetupCon()

	r.Use(func(c *gin.Context) {
		c.Set("db", db)
		c.Next()
	})

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "Welcome to Toko Online API"})
	})

	r.POST("/api/v1/register", controllers.Register)

	r.POST("/api/v1/login", controllers.Login)

	r.Run()
}
